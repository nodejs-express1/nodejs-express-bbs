// var express = require('express');
// var app = express();

const Joi = require('joi') // validate
const express = require('express');
const app = express();

app.use(express.json()); // body 를  json으로 변경 



// var server = app.listen(3000, function()  {
//     console.log(`Server started on port`);
// });


// app.get('/', function(req, res)  {
    //     res.send("hello world");
    // });

const courses = [
    { id : 1, name : 'course1'},
    { id : 2, name : 'course2'},
    { id : 3, name : 'course3'}
]


// api - get
app.get('/', (req, res) => {
    res.send("hi ~~");
});

app.get('/api/courses', (req, res) => {
    res.send(courses);
});

app.get('/api/course/:id', (req, res) => {
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course){ // 404
        res.status(404).send('The course with the given ID was not found.');
        return;
    }
    res.send(course);
});

app.get('/api/posts/:year/:month', (req, res) => {
    res.send(req.query);
});

// api - post
app.post('/api/courses', (req, res) => {

    // validate
    // const schema = {
    //     name : Joi.string().min(3).required()
    // };

    // const result = Joi.validate(req.body, schema);
    // console.log(result);

    // if (result.error) {
    //     res.status(400).send(result.error.details[0].message);
        
    //     return;
    // }

    // validatation code refactor
    const { error } = validateCourse(req.body); // result.error
    if (error) return res.status(400).send(result.error.details[0].message);

    // validate
    // if(!req.body.name || req.body.name.length < 3){
    //     // 400 bad Request
    //     res.status(400).send('Name is required and shoud be minimum 3 characters.');
    //     return;
    // }


    const course = {
        id: courses.length + 1,
        name : req.body.name
    };
    courses.push(course);
    res.send(course);
});


app.put('/api/courses/:id', (req, res) => {
    // Look up the course
    // If not existing, return 404
    const course = courses.find(c => c.id === parseInt(req.params.id));
    if(!course) return res.status(404).send('The course with the given ID was not found.');

    
    // Validate
    // If ivalid, return 400 - Bad request
    // const result = validateCourse(req.body);
    const { error } = validateCourse(req.body); // result.error

    if (error) return res.status(400).send(error.details[0].message);

    // Update course
    course.naem = req.body.name;

    // Return the updated course
    res.send(course);


});


app.delete('/api/courses/:id', (req, res) => {
    // Look up the course

    // Not existing, return 404
    const course = courses.find(c => c.id === parseInt(req.params.id));
    // if(!course){ // 404
    //     res.status(404).send('The course with the given ID was not found.');
    //     return;
    // }

    if(!course) return res.status(404).send('The course with the given ID was not found.');

    // Delete
    const index = courses.indexOf(course);

    courses.splice(index,1);
    // Return the same course
    res.send(course);
});


// common function
function validateCourse(course){
    const schema = {
        name : Joi.string().min(3).required()
    };
    const result = Joi.validate(course, schema);
    console.log(result);

    return result;
}
   
// port
const port = process.env.PORT || 3000;
app.listen(port, () =>  console.log(`Server started on  port ${port}`));